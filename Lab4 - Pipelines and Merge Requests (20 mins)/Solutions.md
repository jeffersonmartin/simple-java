1.1 Check the icon in the pipelines page, the icon in this example is for a branch pipeline 

![Branch pipeline](images/branch.png)

1.2 Create a merge request, and commit to the branch that is associated to this MR. At least one of the jobs should have this rule: `rules: - if: $CI_PIPELINE_SOURCE=="merge_request_event"`

2.1 Enable Merge train and merged results in the project settings 

2.2 [.gitlab-ci.yml - pipeline for merge request](https://gitlab.com/tech-marketing/workshops/cicd/ci-workshop/simple-java/-/snippets/2077764) 

2.3 Click the `Add to merge train button`

2.4 When you submit a merge request, you are requesting to merge changes from a source branch into a target branch. By default, the CI pipeline runs jobs against the source branch.
With pipelines for merged results, the pipeline runs as if the changes from the source branch have already been merged into the target branch. The commit shown for the pipeline does not exist on the source or target branches but represents the combined target and source branches.

2.5 [How merge trains keep your master green](https://about.gitlab.com/blog/2020/01/30/all-aboard-merge-trains/)
