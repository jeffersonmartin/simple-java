# Lab4 - Pipelines and Merge Requests (20 mins)

### In this lab we will cover the following topics:

- Variables 
- Rules 

### Variables and the rules label - Lesson 

[Watch this video ](https://drive.google.com/file/d/1FsapP5_kAPycQje4v_u0AOH9P2pCG4tG/view?usp=sharing)

[Pipelines and Merge Requests](https://docs.google.com/presentation/d/1R4KLFTWFMLaRL_hdIxwgRp37CeYtqQ8Krbf112hbRFo/edit?usp=sharing)

## Exercise 1  - The Branch and merge request pipeline 

1. Run a pipeline, open the pipelines page, in your pipeline in the pipeline list, identify via the icon if it is a branch pipeline or MR pipeline, check the branch name and the commit message 
2. Create a Merge Request Pipeline, check the pipeline icon, and the MR id in the pipelines page.

## Exercise 2 - Pipeline for Merged Results and Merge Trains

1. Enable Merge Train and Merged Results 
2. Run pipeline for Merge results 
3. Start Merge Train 
4. What is pipeline for Merged results? 
4. What is a Merge Train, which problem it solves? 
