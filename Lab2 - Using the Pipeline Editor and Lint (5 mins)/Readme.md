## Lab1 - Pipeline Editor (5 mins)

In this lab we will experiance the Pipeline Editor


### Exercise 1  - Create a basic CI configuration using the Pipeline editor 

1. Open the editor (CICD->Editor), and create three stages and a dummmy hello-world job in each stage
2. Check the Visualize tab, and the Lint tab
3. Add `include: - template: Auto-DevOps.gitlab-ci.yml`, check the `View merged YAML` tab 


