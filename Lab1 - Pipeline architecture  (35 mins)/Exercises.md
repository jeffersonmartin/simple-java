## Lab1 - Pipeline architecture (35 minutes)

### In this lab we will cover the following topics:

- Stages, Jobs , Options
- Directed Acyclic Graph(DAG)
- Parent/Child pipelines
- Multi Project Pipelines
- Dynamic Pipelines
- CI component in Auto Devops: Auto Build job: Herokuish Buildpacks, Cloud Native Buildpacks, Dockerfile


### Pipeline architecture - Lesson 

[Watch this video ](https://drive.google.com/file/d/1bOg88nZsNuSiP6ON5wSD-d_L0duUJi4T/view?usp=sharing)

[Pipeline architecture deck ](https://docs.google.com/presentation/d/1_5JYIqDJaWyW2kXMfTu6A7HHSYlwspKZcssUbEguwGg/edit#slide=id.g8ae6d35cde_0_0)

### Exercise 1  - Create a basic CI configuration 

1. Create a new .gitlab-ci.yml in the root of the repositiry - from the project overview page, click the Plus icon and select New file . 
2. Add the stages `Build`, `Test`, `Deploy`. 
3. Create jobs: `build` under `Build` stage, two test jobs under `Test` stage : `Test-A`, `Test-B`, `Deploy` job under `Deploy` stage. 
4. Each job must include a script and a stage.
5. Set the deploy job to manual - add `when: manual`
6. In the Commit message, add the following message : "Creating .gitlab-ci.yml" 
7. In the Target Branch, create a new branch by typing a branch name: lab1-exc1
8. Check the pipeline graph -> CI/ CD menu -> Pipelines -> open the first pipeline from the list. 


### Exercise 2 - Directed Acyclic Graph  

1. Create another build job in thw build stage, `build-B`. 
2. Change the `test-A` job so it will be able to start run as soon as `build` completes, even before `build-B` completes - add `needs: build`
3. To test that DAG works, add a delay of 5 minutes to `build-B` -> add `when: delayed`, `start_in: 5 minutes`
4. Commit the change.
5. Check the pipeline graph  -> CI/ CD menu -> Pipelines -> open the first pipeline from the list. 


### Exercise 3 - Parent-Child Pipeline 

1. Create a sub directory in the root of the repository - name it `serviceA``.
2. Open the directory, and create in it CI configuration file (`.gitlab-ci.yml`), which includes only one simple job with `script: echo "Any message..." `.
3. In your main CI configuration add a job that triggers the child pipeline you just created with the keyword `trigger`, add `include` to the child CI configuration. 
4. Add `strategy: depend` to this trigger. 
5. Add a rule that the job will run only if there were changes in the directory with the keywords `rules` and `changes`.


### Exercise 4 - Multi-project pipeline 

1. Create a new project, name it `app-frontend`
2. Create in it CI configuration file (`.gitlab-ci.yml`), which includes only one simple job with `script: echo "Any message..." `
3. From your main CI configuration in the original project, create a bridge job that will trigger a pipeline in `app-frontend` , name the job `bridge`, use keywords: `trigger`. `project`, `branch`, `strategy`. The `project` value should include the full path of the downstream project. 


### Exercise 5 - Dynamic child pipeline 

1. In the root of the repository create a CI configuration file which includes a simple job with this script: `script: echo "REPLACEME" `
2. Name this file `replace.gitlab-ci.yml` 
3. In your main CI configuration, create a job named `setup`. The setup job will dynamically create a CI configuration file, and save it as an artifact.  
4. Your job should include this script ` - sed "s/REPLACEME/DynamicPipeline/g" replace.gitlab-ci.yml > generated-config.yml ` . The script will read the text from the file you created in step 1,  will replace the text `REPLACEME` with `DynamicPipeline` and will create a new config `generated-config.yml` 
4. Save the new file `generated-config.yml` as an artifact 
5. Create a trigger job that will use the config you saved as an artifact to trigger dynamic pipeline, use the following keywords: `include`, `artifact` and `job` 
