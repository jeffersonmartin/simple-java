## CI Labs

### Pipeline architecture  

- Stages, Jobs , Options 
- Directed Acyclic Graph(DAG) 
- Parent/Child pipelines
- Multi Project Pipelines 
- Dynamic Pipelines 
- CI component in Auto Devops:
- Auto Build job: Herokuish Buildpacks, Cloud Native Buildpacks, Dockerfile


### Using the pipeline Editor and Lint 

- Pipeline Editor
- CI Lint 

### Controlling how and when your jobs run

- Variables
- Predefined Variables 
- Rules 

### Pipelines and Merge Requests 
 
- The Branch Pipeline 
- The Merge Request Pipeline 
- Pipelines for Merge Results 
- Merge Trains 
- Types of CI/CD pipeline runs 

### Assembling pipelines from components

- include
- extend
- YAML Anchors
- !reference
- customizing Auto DevOps

### Holding, securing, and sharing results

The following is a non-exhaustive list of topics to be covered in this lab:

- Job Artifacts
- Container and Package Registries 

### Tips & Tricks for the power user

- Skip CI
- CI Linting
- Avoid Double Pipelines
- Parallel and Matrix Testing 
- Leverage Runner Caching 


## Step 1: Logging into Demo Systems

1. Open your browser and visit https://gitlabdemo.com  

2. Click on **Redeem Invitation Code**

![](../images/redeem_access_code.png)  

3. Type in the Invitation code and click on **Redeem and Create Account**  

![](../images/redeem_and_create_account.png)  

4. Save your credentials and click on the yellow button for **Gitlab Dashboard**  

![](../images/save_credentials.png)  

5. Login with your credentials 

![](../images/credential_login_1.png)  

6. Click on the **Groups** dropdown in the top navigation and select **Your groups**   

![](../images/my_groups.png)

7. Go into **Training Users**

![](../images/training_users_1.png)

8. Go into your session

![](../images/my_session.png)

9. Locate and click on the **My Test Group** that matches your username and has an **[Owner]** badge next to it

![](../images/my_test_group.png)

10. Select **New Project**  

![](../images/new_project_1.png)  

11. Select **Create from Template**  

![](../images/create_from_template.png)  

12. Click on the **Group** tab above the list of available templates, then locate the **CI Advanced workshop** and click the **Use Template** button  

![](../images/use_template.png)  


