# Lab3 - Controlling how and when your jobs run (20 mins)

### In this lab we will cover the following topics:

- Variables 
- Rules 

### Variables and the rules label - Lesson 

[Watch this video ](https://drive.google.com/file/d/1mdMFa66JLa4cBfd8VoQnddds39Avq0Gp/view?usp=sharing)

[Variables and the rules label ](https://docs.google.com/presentation/d/1LRUBpkuZnWbuBafWOrZs_sTMxAiTETG-o5wdcT2U_74/edit?usp=sharing)

### Exercise 1  - CI Variables 

1. Create a group level varaiable in GitLab UI , call it `First_name`
2. Create a project level variable in GitLab UI, call it `Last_name`
3. Define a variable inside a job in the CI configuration file, call it `Pet_name`
4. Define a global variable in the CI configuration file, call it `Company_name`
3. In the CI configuration file, create a job with a script that prints the value of the variables you defined. 
4. Run the pipeline and check the job log, make sure the correct values available in the log

### Exercise 2 - Run the pipeline with different variables values 

1. Run the same pipeline, but now overwride the variables with different values.  From CI/CD Pipelines, click `Run Pipeline`, and enter **in the UI** the same variables but with different values  
2. Check the job logs that the new values have been used

### Exercise 3 - Predefined Variables 

Create a job that prints the following information: 

    GitLab user name 
    
    Commit message

    GitLab version

    Branch name
    
    The pipeline source 


### Exercise 4 - Job rules

1. Add a job that runs only if the pipeline triggered from the web 
2. Change this job, so it will not run if triggered from the web, but will run in any other case 
3. Change this job, so it will not run if was triggered from the web, run only if there are changes to the CI configuration file, and start manually 

   
