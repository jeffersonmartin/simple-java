# Lab6- Holding, securing, and sharing results/Exercises (15 mins)

### In this lab we will cover the following topics:

- Job Artifacts
- Container & Package Registries

### Holding, securing, and sharing results/Exercises - Lesson 

[Watch this video ](https://drive.google.com/file/d/1TeRXFg8TJ7au5V-lGswCYvoZkPPJcZwf/view?usp=sharing)

[Deck - Holding, securing, and sharing results](https://docs.google.com/presentation/d/1oEY7tI5m6sk_5QKa-HmWQlQccROsM8svJ4dc_GtgQKs/edit?usp=sharing)

### Exercise 1  - Artifacts

1. Create a job in `build` stage that creates a file, with text `Hello World`, saves this file as an Artifact, and set `expire_in` `1 week` to this artifact.
2. Create a job in `test` stage, that uses this artifact, and prints text in the artifact to the console.   
3. Why do we need to use job artifacts? 



