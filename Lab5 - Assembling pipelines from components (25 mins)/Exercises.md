# Lab5- Assembling pipelines from components (25 mins) 

### In this lab we will cover the following topics:

- include
- extend 
- YAML Anchors
- !reference
- customizing Auto DevOps 

### Assembling pipelines from components - Lesson 

[Watch this video ](https://drive.google.com/file/d/12tKtl7fdRNM6ddlip2gDqT7Y5dyfkaLL/view?usp=sharing)

[Assembling pipelines from components](https://docs.google.com/presentation/d/1xgACMlhy91vjKNBjyq3A6SpE8CFD_wO7Xb29y51t7qM/edit?usp=sharing)

### Exercise 1  - Includes 

1. Create CI configuration file, call it `testing-gitlab-ci.yml`. Add two dummy hello world jobs to it. In your main CI configuration file (`.gitlab-ci.yml`)  include `testing-gitlab-ci.yml`. Run the pipeline and check that the jobs in the include file executed 
2. (Optional) Create another project, with CI configuration file, save the file under templates/.gitlab-ci-template.yml. In your main CI configuration, include this template 
3. Include an official GitLab CI template in your pipeline 


### Exercise 2 - Extends , hidden jobs, named jobs 

1. Create a template job, add "hello-world" script and a rule. Create two jobs that extends the template job you created. Each job has a different script and different rule. Run the pipeline and check the log. 
2. Include Auto DevOps template, extend the `build` job in it by creating a new job with the same name (`build`), add a simple echo script to it. Run the pipeline, check the log
3. Include Auto DevOps template, extend the `build` job with the `extends` keyword. (You will notice that this job name must be different than `build`). Run the pipeline, check the log


### Exercise 3 - YAML Anchors 

1. Create a dummy hello-world hidden job template with YAML anchor , create two jobs that inherit this job template 
2. Create YAML anchor for scripts, create YAML anchor for variables, create a job that uses the scripts and variables from the YAML ancjors you created 

### Exercise 4 - !reference

1. Create CI configuration, call it `setup.yml`, add to `setup.yml` hidden job that has a script, two variables `VAR_A`, `VAR_B`. 
2. In your main CI configuration, include `setup.yml`
3. Create a job that will merge the script defined in `setup.yml`
4. Create a job that prints to the console the value of `VAR_A` and `VAR_B`
5. Define new variable `MY_VAR` that gets the value of `VAR_A` and prints it to the console.


### Exercise 5 - Customizing Auto DevOps 

1. Create CI configuration that include the Auto DevOps template, Disable SAST, DAST and Container Scanning 

