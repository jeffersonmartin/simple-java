1.1  [.gitlab-ci.yml - include CI configuration](https://gitlab.com/tech-marketing/workshops/cicd/ci-workshop/simple-java/-/snippets/2078434)

1.3 [.gitlab-ci.yml - include GitLab template](https://gitlab.com/tech-marketing/workshops/cicd/ci-workshop/simple-java/-/snippets/2079961/) 

2.1 [.gitlab-ci.yml - Extends](https://gitlab.com/tech-marketing/workshops/cicd/ci-workshop/simple-java/-/snippets/2080005)

2.2 [.gitlab-ci.yml - extend Auto DevOps job with the same job name](https://gitlab.com/tech-marketing/workshops/cicd/ci-workshop/simple-java/-/snippets/2080003)

2.3 [.gitlab-ci.yml - extend Auto DevOps job with extends keyword](https://gitlab.com/tech-marketing/workshops/cicd/ci-workshop/simple-java/-/snippets/2080001)

3.1 [.gitlab-ci.yml - YAML Anchors](https://gitlab.com/tech-marketing/workshops/cicd/ci-workshop/simple-java/-/snippets/2080023) 

3.2 [.gitlab-ci.yml - YAML Anchors for scripts and variables](https://gitlab.com/tech-marketing/workshops/cicd/ci-workshop/simple-java/-/snippets/2080025)

4.1 [.gitlab-ci.yml - `setup.yml` with hidden job](https://gitlab.com/tech-marketing/workshops/cicd/ci-workshop/simple-java/-/snippets/2100085)

4.2 [.gitlab-ci.yml - include `setup.yml`](https://gitlab.com/tech-marketing/workshops/cicd/ci-workshop/simple-java/-/snippets/2100086)

4.3 [.gitlab-ci.yml - merge script section from the template job](https://gitlab.com/tech-marketing/workshops/cicd/ci-workshop/simple-java/-/snippets/2100087)

4.4 [.gitlab-ci.yml - merge and print variables](https://gitlab.com/tech-marketing/workshops/cicd/ci-workshop/simple-java/-/snippets/2080350) 

4.5 [.gitlab-ci.yml - reference to a specific variable](https://gitlab.com/tech-marketing/workshops/cicd/ci-workshop/simple-java/-/snippets/2080354)

5   [.gitlab-ci.yml - customize Auto DevOps](https://gitlab.com/tech-marketing/workshops/cicd/ci-workshop/simple-java/-/snippets/2100093)


